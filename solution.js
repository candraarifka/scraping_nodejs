const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');
// const sqlite3 = require('sqlite3')
// let db = new sqlite3.Database("./mydb.sqlite3")

const items = [{cat_id:1, pages:10, category:'Travel & Entertainment'},
            {cat_id:2, pages:7, category:'Lifestyle & Wellness'},
            {cat_id:3, pages:21, category:'F & B'},
            {cat_id:4, pages:1, category:'Gadget & Electronics'},
            {cat_id:5, pages:0, category:'Daily Needs & Home Appliances'},
            {cat_id:6, pages:2, category:'Other Info'}]


function getAlldata(){

    for (item of items){
        let cat_id = item.cat_id
        let page = item.pages
        let category = item.category
        
        let limit = 1
        let new_list = [];
        while(limit <= page){
            let x = [];
            // console.log("page... "+limit)
            let url = 'https://www.bankmega.com/ajax.promolainnya.php?product=&subcat='+cat_id+'&page='+limit+'#'
            request(url, (error,
                response, html) =>{
                    if (!error && response.statusCode == 200){
                        const $ = cheerio.load(html);
                        let y = $('.clearfix LI');
                        let arr = Array.from(y)
                        
                        const lists = [];
                        for(x of arr){
                            const data = {};
                            const info = {}
                            const img = $(x).find('a img')
                            const link = $(x).find('a')
                            category = category;
                            info.imageUrl = 'https://www.bankmega.com/'+img.attr('src');
                            info.title = img.attr('title');
                            info.linkUrl = 'https://www.bankmega.com/'+link.attr('href');
                            data.category = category;
                            data.data = info
                            lists.push(data);
                            // db.run('INSERT INTO bank_mega (cat_id,title,image,link) VALUES (?,?,?,?)', [cat_id, info.title, info.imageUrl , info.linkUrl]);
                        }
    
                        // console.log(lists)


                        // const lists = [];
                        // $('.clearfix LI').each((i, x) => {
                        //     const data = {};
                        //     const info = {}               
                        //     const img = $(x).find('a img')
                        //     const link = $(x).find('a')
                        //     category = category;
                        //     info.imageUrl = 'https://www.bankmega.com/'+img.attr('src');
                        //     info.title = img.attr('title');
                        //     info.linkUrl = 'https://www.bankmega.com/'+link.attr('href');
                        //     data.category = category;
                        //     data.data = info
                        //     lists.push(data);
                        // });
                        // console.log(lists)

                        let data = {}
                        data.all_data = lists

                        if (lists){
                            fs.writeFile ("solution.json", JSON.stringify(data), function(err) {
                                if (err) throw err;
                                console.log('updated');
                                }
                            );
                        };
                        


                    }
                });
          limit++;
        }
    }

};
getAlldata();




