const sqlite3 = require('sqlite3')

let db = new sqlite3.Database("./mydb.sqlite3", (err) => { 
    if (err) { 
        console.log('Error when creating the database', err) 
    } else { 
        console.log('Database created!') 
        /* Put code to create table(s) here */
        createTable()
    } 
})


const createTable = () => {
    console.log("create database table contacts");
    db.run("CREATE TABLE IF NOT EXISTS bank_mega(id INTEGER PRIMARY KEY AUTOINCREMENT, cat_id INTEGER, title TEXT, image TEXT, link TEXT)");
}